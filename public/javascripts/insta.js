"use strict";

var xhr = new XMLHttpRequest();
var xhrInit = new XMLHttpRequest();
if(window.location.hash){
	document.body.removeChild(document.getElementById("login"));
	var access_token = window.location.hash.split('=')[1];
	xhrInit.open("get", "/init/" + access_token, true);
	xhrInit.send();

	var input = document.createElement("input");
	input.type = "text";
	input.id = "user";

	var button = document.createElement("button");
	button.onclick = function(){
		var user = document.getElementById("user").value;
		xhr.open("get", "/getMedia/" + user + "?count=5", true);
		xhr.onload = function(){
			try{
				var data = JSON.parse(this.responseText);
			} catch(e) {
				return;
			}
			makeList(data);
		}
		xhr.send();
	};
	button.innerText = "click here to get data";
	document.body.appendChild(input);
	document.body.appendChild(button);
}

function makeList(data){
	var items = data.data;
	var next_max_id = data.pagination.next_max_id;
	var list = document.getElementById("items_list");
	if(!list){
		list = document.createElement("ul");
		list.id = "items_list";
	}
	for(let li in items){
		let el = document.createElement("li");
		let img = document.createElement("img");
		let anchor = document.createElement("a");
		anchor.href = "/media/" + items[li].id;
		img.src = items[li].images.thumbnail.url;
		anchor.appendChild(img);
		el.innerText = items[li].caption ? items[li].caption.text : "no caption";
		el.appendChild(anchor);
		list.appendChild(el);
	}
	document.body.appendChild(list);
	var button = document.getElementById("more_button");
	if(document.getElementById("more_button")){
		document.body.removeChild(document.getElementById("more_button"));
	}
	if(!next_max_id) return;
	var button = document.createElement("button");
	button.innerText = "more";
	button.id = "more_button"
	button.onclick = function(){
		var user = document.getElementById("user").value;
		xhr.open("get", "/getMedia/" + user + "?count=5" + "&max_id=" + next_max_id, true);
		xhr.onload = function(){
			var data = JSON.parse(this.responseText);
			makeList(data);
		}
		xhr.send();
	}
	document.body.appendChild(button);
}


