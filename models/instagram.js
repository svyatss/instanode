var instagramApi = require('instagram-api');
var instagram = false;

exports.init = function(at){
	if(!instagram){
		instagram = new instagramApi(at);
	}
}

exports.getMedia = function(user, options, callback){
	instagram.userSearch(user).then(function(result){
		var id = result.data[0].id;
		instagram.userMedia(id, options).then(callback);
	});
}

exports.media = function(id, callback){
	instagram.media(id).then(callback);
}
