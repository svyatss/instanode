var express = require('express');
var router = express.Router();
var instagram = require('../controllers/instagram');
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/getMedia/:id', instagram.getMedia);

router.get('/init/:at', instagram.init);

router.get('/media/:id', instagram.media);

module.exports = router;
