var request = require('request');
var url = require('url');
var instagram = require('../models/instagram');

exports.init = function(req, res){
	var at = url.parse(req.url).pathname.replace("/init/", "");
	instagram.init(at);
	res.send('ok');
}

exports.getMedia = function(req, res){
	var user = url.parse(req.url).pathname.replace("/getMedia/", "");
	var options = req.query;
	instagram.getMedia(user, options, function(result){
		res.send(result);
	});
}

exports.media = function(req, res, next){
	var id = url.parse(req.url).pathname.replace("/media/", "");
	instagram.media(id, function(result){
		var img = result.data.images.standard_resolution.url;
		var caption = result.data.caption ? result.data.caption.text : "no caption";
		var likes = result.data.likes.count;
		var comments = result.data.comments.count;
		res.render('media', {img: img, caption: caption, likes: likes, comments: comments});
	});
}